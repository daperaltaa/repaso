<?php
$Titulo = "";
if (isset($_POST["Titulo"])) {
    $Titulo = $_POST["Titulo"];
}

$Descripcion = "";
if (isset($_POST["Descripcion"])) {
    $Descripcion = $_POST["Descripcion"];
}

$Fecha = "";
if (isset($_POST["Fecha"])) {
    $Fecha = $_POST["Fecha"];
}

if (isset($_POST["crear"])) {
    $articulo=new articulo("",$Titulo,$Descripcion,$Fecha);
    $articulo->insertar();

}
?>

<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-info">
					<h4>Crear Articlo</h4>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["crear"])){ ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						Datos ingresados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
					<form action="index.php?pid=<?php echo base64_encode("presentacion/articulo/registrar.php") ?>" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label>Titulo</label> 
							<input type="text" name="Titulo" class="form-control" value="<?php echo $Titulo ?>" required>
						</div>
						<div class="form-group">
							<label>Descripcion</label> 
							<input type="text" name="Descripcion" class="form-control" min="1" value="<?php echo $Descripcion ?>" required>
						</div>
						<div class="form-group">
							<label>Fecha</label> 
							<input type="date" name="Fecha" class="form-control" min="1" value="" required>
						</div>
						
						<button type="submit" name="crear" class="btn btn-info">Crear <i class="fas fa-plus-circle"></i></button>
					</form>
            	</div>
            </div>
		</div>
	</div>
</div>