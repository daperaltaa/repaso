<?php 
require_once "persistencia/conexion.php";
require_once "persistencia/articuloDAO.php";

class articulo{
    private $id;
    private $titulo;
    private $descripcion;
    private $fecha;
    private $conexion;
    private $articuloDAO;
    
    public function getId()
    {
        return $this->id;
    }

    public function getTitulo()
    {
        return $this->titulo;
    }

    public function getDescripcion()
    {
        return $this->descripcion;
    }

    public function getFecha()
    {
        return $this->fecha;
    }

    
    public function articulo($id="",$titulo="",$descripcion="",$fecha=""){
        $this->id=$id;
        $this->titulo=$titulo;
        $this->descripcion=$descripcion;
        $this->fecha=$fecha;
        $this -> conexion = new conexion();
        $this -> articuloDAO = new articuloDAO($this->id,$this->titulo,$this->descripcion,$this->fecha);
    }
    
    public function insertar(){
        $this -> conexion -> abrir();
        echo $this -> articuloDAO -> insertar();
        $this -> conexion -> ejecutar($this -> articuloDAO -> insertar());
        $this -> conexion -> cerrar();
    }
    
    
    public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> articuloDAO -> consultarPaginacion($cantidad, $pagina));
        $articulos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new articulo($resultado[0], $resultado[1], $resultado[2], $resultado[3]);
            array_push($articulos, $p);
        }
        $this -> conexion -> cerrar();
        return $articulos;
    }
    
    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> articuloDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
    
    
}

?>